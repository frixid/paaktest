var express = require("express");
var router = express.Router();
var data = require("../data/delivery");
/* GET delivery*/
router.post("/", function(req, res, next){
  var delivery = data.delivery.find(item => item.id === req.body.id);
  res.send(delivery);
});

module.exports = router;

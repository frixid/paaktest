var express = require('express');
var router = express.Router();
var data = require('../data/deliveries')
/* GET deliveries */
router.get('/', function(req, res, next) {
  res.send(data.deliveries);
});

module.exports = router;

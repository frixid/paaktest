const deliveries = [
  {
    id: 12394,
    address: "Some address",
    latitude: 2.334455,
    longitude: 4.323423,
    customer_name: "Juan Perez"
  },
  {
    id: 4567,
    address: "Some other address",
    latitude: 2.3432167,
    longitude: 4.676453,
    customer_name: "María Castro"
  }
]

module.exports ={
  deliveries
}

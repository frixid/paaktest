const delivery =
[  {
    id: 12394,
    address: "Some address",
    latitude: 2.334455,
    longitude: 4.323423,
    customer_name: "JuanPérez",
    requires_signature: false,
    special_instructions:
      "blue door, besides the car shop. Deliver to the car shop if necessary"
  },
  {
    id: 4567,
    address: "Some address",
    latitude: 2.334455,
    longitude: 4.323423,
    customer_name: "María Castro",
    requires_signature: true,
    special_instructions:
      "blue door, besides the car shop. Deliver to the car shop if necessary"
  }
]

module.exports = {
  delivery
}

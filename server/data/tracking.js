const tracking = [
  {
    driver_id: 554,
    tracking_data: [
      {
        Latitude: 1.939324,
        longitude: 2.30034,
        delivery_id: 1234,
        battery_level: 93,
        timestamp: 2342123433
      },
      {
        Latitude: 1.76667,
        longitude: 2.3333,
        delivery_id: 1234,
        battery_level: 91,
        timestamp: 2342125555
      },
      {
        Latitude: 1.939324,
        longitude: 2.30034,
        delivery_id: 4567,
        battery_level: 85,
        timestamp: 234216666
      }
    ]
  }
];

module.exports = {
  tracking
};

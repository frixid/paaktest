import { registerRootComponent } from "expo";
import React from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, combineReducers } from "redux";
import ListScreen from "./screens/Deliveries.js";
import DetailsScreen from "./screens/Details.js";
import reducers from "./constants/reducers";
import thunkMiddleware from "redux-thunk";

import {
  SafeAreaView,
  Button,
  View,
  Text,
  Dimensions,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { createAppContainer, withNavigation } from "react-navigation";
import {
  DrawerItems,
  NavigationDrawerProp,
  createDrawerNavigator
} from "react-navigation-drawer";

const store = createStore(
  combineReducers({ ...reducers }),
  applyMiddleware(thunkMiddleware)
);

const { width } = Dimensions.get("window");
const Drawer = createDrawerNavigator(
  {
    List: {
      screen: ListScreen,
      navigationOptions: () => ({
        title: "Deliveries",
        drawerLabel: "Deliveries"
      })
    },
    Details: {
      screen: DetailsScreen,
      navigationOptions: () => ({
        title: "Current track",
        drawerLabel: "Current track"
      })
    }
  },
  {
    drawerPosition: "left",
    drawerWidth: (width / 3) * 2,
    initialRouteName: "List",
    contentOptions: {
      activeTintColor: "#e91e63"
    }
  }
);

const App = createAppContainer(Drawer);

export default class RootComponent extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

registerRootComponent(RootComponent);

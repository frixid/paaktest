import ActionTypes from "./constants/actionTypes";
import axios from "axios";
import * as Battery from "expo-battery";
import * as Location from "expo-location";
import * as BackgroundFetch from "expo-background-fetch";
import * as TaskManager from "expo-task-manager";

let LOCATION_TASK = "LOCATION_TASK";
let FETCH_TASK = "FETCH_TASK";
let INTERVAL = 5;

export const showDeliveries = () => {
  return async dispatch => {
    try {
      const response = await axios.get("http://localhost:3000/deliveries");
      dispatch({
        type: ActionTypes.SHOW_DELIVERIES,
        payload: {
          deliveries: response.data
        }
      });
    } catch (error) {
      console.error(error);
    }
  };
};
export const showDelivery = id => {
  return async dispatch => {
    try {
      const response = await axios.post("http://localhost:3000/delivery", {
        id: id
      });
      dispatch({
        type: ActionTypes.SHOW_DELIVERY,
        payload: {
          delivery: response.data
        }
      });
    } catch (error) {
      console.error(error);
    }
  };
};
export const activeDelivery = id => {
  return async dispatch => {
    var batt = await Battery.getBatteryLevelAsync();
    var perm = Location.requestPermissionsAsync();
    var location = await Location.getLastKnownPositionAsync();

    function range(old_value) {
      return ((old_value - 0) / (1 - 0)) * (100 - 0) + 0;
    }

    var tracking = {
      Latitude: location.coords.latitude,
      longitude: location.coords.latitude,
      delivery_id: id,
      battery_level: range(batt),
      timestamp: location.timestamp
    };

    try {
      const response = await axios.post(
        "http://localhost:3000/tracking",
        tracking
      );
      console.log(response.data);
      dispatch({
        type: ActionTypes.ACTIVE_DELIVERY,
        payload: {
          deliveryActive: id
        }
      });
    } catch (error) {
      console.log(error);
    }
  };
};

export async function registerFetchTask() {
  const status = await BackgroundFetch.getStatusAsync();
  switch (status) {
    case BackgroundFetch.Status.Restricted:
    case BackgroundFetch.Status.Denied:
      console.log("Background fetch disabled");
      return;
    case BackgroundFetch.Status.Available:
      let tasks = await TaskManager.getRegisteredTasksAsync();
      await BackgroundFetch.registerTaskAsync(FETCH_TASK);
      tasks = await TaskManager.getRegisteredTasksAsync();
      console.log(tasks);
      if (tasks.find(f => f.taskName === FETCH_TASK) == null) {
        await BackgroundFetch.registerTaskAsync(FETCH_TASK);
        tasks = await TaskManager.getRegisteredTasksAsync();
      }
      await BackgroundFetch.setMinimumIntervalAsync(INTERVAL);
      return;
  }
}
export async function locationTask() {
  await Location.requestPermissionsAsync();
  await Location.startLocationUpdatesAsync(LOCATION_TASK, {
    accuracy: Location.Accuracy.BestForNavigation,
    timeInterval: 1000,
    distanceInterval: 0,
    deferredUpdatesInterval: 0,
    deferredUpdatesDistance: 0,
    pausesUpdatesAutomatically: false
  });
}
async function unregisterAllTasks() {
  await TaskManager.unregisterAllTasksAsync();
}

TaskManager.defineTask(FETCH_TASK, async () => {
  try {
    const receivedNewData = await axios.post("http://localhost:3000/tracking");
    return BackgroundFetch.Result.NewData;
  } catch (error) {
    return BackgroundFetch.Result.Failed;
  }
});
TaskManager.defineTask(LOCATION_TASK, async ({ data, error }) => {
  if (error) {
    console.log(error);
    return;
  }
  if (data) {
    try {
      const response = await axios.post(
        "http://localhost:3000/tracking",
        data.locations
      );
      console.log(response.data);
    } catch (error) {
      console.log(error);
    }
  }
});

unregisterAllTasks();

locationTask();
registerFetchTask();

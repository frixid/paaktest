import React, { useEffect } from "react";
import * as actions from "../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  SafeAreaView,
  TouchableHighlight
} from "react-native";

function details(props) {
  const { actions, deliveryActive, delivery } = props;

  const handleActive = () => {
    props.navigation.navigate("List");
    actions.activeDelivery(delivery.id);
  };
  const handleActived = () => {
    props.navigation.navigate("List");
  };

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.item}>Name:{delivery.customer_name}</Text>
      <Text style={styles.item}>Address: {delivery.address}</Text>
      <Text style={styles.item}>Latitude: {delivery.latitude}</Text>
      <Text style={styles.item}>Longitude: {delivery.longitude}</Text>
      <Text style={styles.item}>
        Require signature: {!delivery.requires_signature ? "yes" : "no"}
      </Text>
      <Text style={styles.item}>
        Special Instructions: {delivery.special_instructions}
      </Text>

      {deliveryActive !== delivery.id ? (
        <Button title="Active" onPress={handleActive} />
      ) : (
        <Button title="Actived" onPress={handleActived} />
      )}
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "stretch",
    marginLeft: 20,
    marginRight: 20
  },

  item: {
    fontWeight: "bold",
    color: "red",
    fontSize: 20
  }
});
const mapStateToProps = state => ({
  deliveryActive: state.appReducer.deliveryActive,
  delivery: state.appReducer.delivery
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(details);

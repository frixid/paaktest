import React, { useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../actions";
import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  SafeAreaView,
  TouchableHighlight
} from "react-native";

function App(props) {
  const { deliveries } = props;
  useEffect(() => {
    props.actions.showDeliveries();
  }, []);

  const handleDelivery = (e,id) => {
    props.actions.showDelivery(id)
    props.navigation.navigate("Details")
  };

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={deliveries}
        renderItem={({ item }) => (
          <TouchableHighlight
            onPress={(e)=>handleDelivery(e,item.id)}
          >
            <Text style={styles.item}>{item.customer_name}</Text>
          </TouchableHighlight>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "stretch",
    justifyContent: "space-around",
    marginLeft: 20,
    marginRight: 20
  },
  item: {
    fontWeight: "bold",
    color: "red",
    margin: 5,
    padding: 10,
    fontSize: 20
  }
});
const mapStateToProps = state => ({
  deliveries: state.appReducer.deliveries
});
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

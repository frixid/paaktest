import ActionTypes from "./constants/actionTypes";

const initialState = {
  deliveries: [],
  delivery: "",
  deliveryActive: null
};

export default function appReducer(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.SHOW_DELIVERIES:
      return {
        ...state,
        deliveries: action.payload.deliveries
      };
    case ActionTypes.SHOW_DELIVERY:
      return {
        ...state,
        delivery: action.payload.delivery
      };
    case ActionTypes.ACTIVE_DELIVERY:
      return {
        ...state,
        deliveryActive: action.payload.deliveryActive
      };
    default:
      return state;
  }
}
